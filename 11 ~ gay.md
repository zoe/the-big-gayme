# 11 ~ gay

i strongly want to create a very gay story

I've been wanting to create a side scroller stealth game, a la dead cells with stealth and entirely different gameplay.
I'm thinking of making a romantic story with this game, where the main character switches between two different dimensions, and 


# story
## concept
slightly persona style, in that it switches between normal life and secret life. Main character gets to know this girl who she begins to really like. When she finally tells her how she feels, she gets turned down. The love interest stops showing up to school, and the main character has to convince herself to forget about her. occasional visits to dimension #2 happen

### love
Eventually, however, the main character no longer has to forget about the girl. When you're finally about to kill the final boss, a curveball gets thrown at you. You find the love interest. She's been imprisoned. You learn that she was the daughter of the final boss. She initially befriended you so that she could spy on you, but she also grew feelings for you. When you confessed your love for her, she began wanting to run away from her father, so that you could be together.

After you see each other and share a meaningful glimpse or whatever, the game changes perspective over to Penny for a bit. this is when you get introduced to her powers, which you use to escape from captivity. 

maybe there could be several perspective shifts as both are trying to get to the other. first a mission with Penny, sneaking out of her prison, then a mission with Nova breaking down the doors and blasting her way in. then back to Penny, making use of the commotion remain unnoticed.

eventually, maybe Nova gets captured by the boss and Penny has to save her, perhaps culminating in a boss fight where you have to use both Penny and Nova's abilities (introducing you to what will make up much of the gameplay in the future; playing both at the same time)

### part 2
When you save her, the game becomes much longer. The final boss relocates, buffs his defenses. Nova and Penny are now together, and much stronger than either of them were alone. When you enter dimension #2, you can fuse into each other, making you into one, and this makes you able to use both of their powers.

### part 3?
other thought for the story; perhaps it should be split into three parts? part one; you have only Nova,

part two; you have Nova and Penny, 

part three; some twist happens which makes the main character and gf take to the real world. it turns out villain is doing bad shit irl too. 
maybe the creatures from dimension #2 come into your dimension to help you take out the evil rich guy or smth

## gameplay

### concept

The main characters both have access to dimension #1 and dimension #2.

### characters

The main characters, let's call them uhhhh Nova and Penny, will be different:

#### Nova
     - _stronger, more offensive. Hard-hitting weaponry; Loud guns, hammers, steel bats._
     - _ability(?): mabe she can activiate a forcefield that can absorb bullets,a dn then unleas them back at enemies_
     - _gets stronger when she's in plain sight_
     - _dog_

#### Penny
     - _More athletic, stealthy. Tactical weaponry; Traps, gadgets, etc._
     - _ability(?): can cloak and slow down everything except herself._
     - _is stronger when she's in the dark_
     - _cat/raven/magpie_
		 
### dimensions

#### Dimension #1 - real world

_Isometric world for the player to explore. Starts out as vibrant and colourful adn full of life, but gradually becomes duller and sadder and greyer. By the end of the game, the player will revert this_

#### Dimension #2

_An alternate world, with a different group of nice ppl who Nova watches slowly get more and more exploited by the villain. Nova accidentally discovers this dimension and befriends the ppl there long before the villain comes into the picture._
